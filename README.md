userscripts
===========

A selection of useful userscripts to be run with Greasmonkey or Tampermonkey:

- yt_mp3.user.js - userscript to add a button to convert YouTube videos to mp3 using video2mp3.net.
- yt_sub.user.js - userscript to switch the YouTube homepage to your subscriptions view.
- yt_popout.user.js - userscript to add a popout button on YouTube videos.
- feedly_yt_popout.user.js - userscript to add a popout button on Feedly YouTube feeds.
- pdf_save.user.js - userscript that allows the ctrl-s keybinding to save PDFs on [PDFescape](http://www.pdfescape.com).
